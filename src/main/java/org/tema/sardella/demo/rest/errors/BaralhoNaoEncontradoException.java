package org.tema.sardella.demo.rest.errors;

public class BaralhoNaoEncontradoException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public BaralhoNaoEncontradoException() {
        super("Carta n�o encontrada.");
    }

}
