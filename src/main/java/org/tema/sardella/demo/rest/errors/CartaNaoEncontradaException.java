package org.tema.sardella.demo.rest.errors;

public class CartaNaoEncontradaException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public CartaNaoEncontradaException() {
        super("Carta n�o encontrada.");
    }

}
