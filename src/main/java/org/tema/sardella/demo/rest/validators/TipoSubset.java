package org.tema.sardella.demo.rest.validators;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import org.tema.sardella.demo.domain.enums.Tipo;

@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = TipoSubsetValidator.class)
public @interface TipoSubset {
    Tipo[] anyOf();
    String message() default "deve ser de um dos tipos {anyOf}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}