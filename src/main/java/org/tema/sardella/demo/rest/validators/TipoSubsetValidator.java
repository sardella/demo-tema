package org.tema.sardella.demo.rest.validators;

import java.util.Arrays;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.tema.sardella.demo.domain.enums.Tipo;

public class TipoSubsetValidator implements ConstraintValidator<TipoSubset, Tipo> {
    private Tipo[] subset;

    @Override
    public void initialize(TipoSubset constraint) {
        this.subset = constraint.anyOf();
    }

    @Override
    public boolean isValid(Tipo value, ConstraintValidatorContext context) {
        return value == null || Arrays.asList(subset).contains(value);
    }
}