package org.tema.sardella.demo.rest;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.tema.sardella.demo.domain.enums.Classe;
import org.tema.sardella.demo.domain.enums.Tipo;
import org.tema.sardella.demo.rest.errors.ApiError;
import org.tema.sardella.demo.rest.errors.CartaNaoEncontradaException;
import org.tema.sardella.demo.service.CartaService;
import org.tema.sardella.demo.service.dto.CartaDTO;

@RestController
@RequestMapping("/api/v0/cartas")
public class CartaResource {

	private final CartaService cartaService;

	public CartaResource(CartaService cartaService) {
		this.cartaService = cartaService;
	}
	

	@PostMapping()	
	public ResponseEntity<Object> createCarta(@Valid @RequestBody CartaDTO cartaDTO) throws URISyntaxException {

		try {
			
			return new ResponseEntity<>(cartaService.createCarta(cartaDTO), HttpStatus.CREATED);
			
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Object> getCartaById(@PathVariable Long id) {
		try {
			final Optional<CartaDTO> carta = cartaService.getOne(id);
			return new ResponseEntity<>(carta.get(), HttpStatus.OK);
		} catch (EntityNotFoundException e) {
			ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, "Carta n�o encontrada.", "");
			return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	@GetMapping()
	public ResponseEntity<Object> getCartas(Pageable pageable, @RequestParam(required = false, value = "nome") String nome,
			@RequestParam(required = false, value = "classe") Classe classe, @RequestParam(required = false, value = "tipo") Tipo tipo) {

		try {

			final Page<CartaDTO> page = cartaService.getCartas(pageable, nome, classe, tipo);

			if(page.isEmpty()) {
				ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, "Nenhuma Carta encontrada.", "");
				return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
			}
			return new ResponseEntity<>(page.getContent(), HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> deleteCarta(@PathVariable Long id) {

		try {

			cartaService.deleteCarta(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);

		} catch (CartaNaoEncontradaException e) {
			ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, "Carta n�o encontrada.", "");
			return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
