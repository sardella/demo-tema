package org.tema.sardella.demo.rest;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.tema.sardella.demo.domain.enums.Classe;
import org.tema.sardella.demo.rest.errors.ApiError;
import org.tema.sardella.demo.rest.errors.BaralhoNaoEncontradoException;
import org.tema.sardella.demo.rest.errors.LimiteBaralhoExcedidoException;
import org.tema.sardella.demo.service.BaralhoService;
import org.tema.sardella.demo.service.dto.BaralhoDTO;

@RestController
@RequestMapping("/api/v0/baralhos")
public class BaralhoResource {

	private final BaralhoService baralhoService;

	public BaralhoResource(BaralhoService baralhoService) {
		this.baralhoService = baralhoService;
	}
	

	@PostMapping()	
	public ResponseEntity<Object> createBaralho(@Valid @RequestBody BaralhoDTO baralhoDTO) throws URISyntaxException {

		try {
			
			return new ResponseEntity<>(baralhoService.createBaralho(baralhoDTO), HttpStatus.CREATED);
			
		} catch (LimiteBaralhoExcedidoException e) {
			ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, "O Baralho s� pode ter no m�ximo 30 cartas.", "");
			return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Object> getBaralhoById(@PathVariable Long id) {
		try {
			final Optional<BaralhoDTO> baralho = baralhoService.getOne(id);
			return new ResponseEntity<>(baralho.get(), HttpStatus.OK);
		} catch (EntityNotFoundException e) {
			ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, "Baralho n�o encontrado.", "");
			return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	@GetMapping()
	public ResponseEntity<Object> getBaralhos(Pageable pageable, @RequestParam(required = false, value = "nome") String nome,
			@RequestParam(required = false, value = "classe") Classe classe) {

		try {

			final Page<BaralhoDTO> page = baralhoService.getBaralhos(pageable, nome, classe);

			if(page.isEmpty()) {
				ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, "Nenhum Baralho encontrado.", "");
				return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
			}
			return new ResponseEntity<>(page.getContent(), HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> deleteBaralho(@PathVariable Long id) {

		try {

			baralhoService.deleteBaralhlo(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);

		} catch (BaralhoNaoEncontradoException e) {
			ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, "Baralho n�o encontrado.", "");
			return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
