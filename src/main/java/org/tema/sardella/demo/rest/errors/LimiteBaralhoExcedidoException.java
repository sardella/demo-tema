package org.tema.sardella.demo.rest.errors;

public class LimiteBaralhoExcedidoException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public LimiteBaralhoExcedidoException() {
        super("Carta n�o encontrada.");
    }

}
