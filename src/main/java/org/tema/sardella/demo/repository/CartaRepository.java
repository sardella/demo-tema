package org.tema.sardella.demo.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.tema.sardella.demo.domain.Carta;
import org.tema.sardella.demo.domain.enums.Classe;
import org.tema.sardella.demo.domain.enums.Tipo;

@Repository
public interface CartaRepository extends JpaRepository<Carta, Long> {

    Page<Carta> findAll(Pageable pageable);
        
    Page<Carta> findByNomeContainingIgnoreCase(Pageable pageable, String nome);
    
    Page<Carta> findByClasse(Pageable pageable, Classe classe);
   
    Page<Carta> findByTipo(Pageable pageable, Tipo tipo);
    
    Optional<Carta> findById(Long id);
    
}
