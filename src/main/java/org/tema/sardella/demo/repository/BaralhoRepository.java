package org.tema.sardella.demo.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.tema.sardella.demo.domain.Baralho;
import org.tema.sardella.demo.domain.enums.Classe;

@Repository
public interface BaralhoRepository extends JpaRepository<Baralho, Long> {

    Page<Baralho> findAll(Pageable pageable);
        
    Page<Baralho> findByNomeContainingIgnoreCase(Pageable pageable, String nome);
    
    Page<Baralho> findByClasse(Pageable pageable, Classe classe);
   
    Optional<Baralho> findById(Long id);
    
}
