package org.tema.sardella.demo.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.tema.sardella.demo.domain.enums.Classe;
import org.tema.sardella.demo.domain.enums.Tipo;

import com.sun.istack.NotNull;

@Entity
@Table(name = "carta")
public class Carta  implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 1, max = 30)
    @Column(nullable = false)
    private String nome;

    @NotNull
    @Size(min = 1, max = 100)
    @Column(nullable = false)
    private String descricao;

    @NotNull
    @Min(0)
    @Max(10)
    @Column(nullable = false)
    private Integer ataque;
   
    @NotNull
    @Min(0)
    @Max(10)
    @Column(nullable = false)
    private Integer defesa;
    
    @Enumerated(EnumType.ORDINAL)
    @NotNull
    private Tipo tipo;

    @NotNull
    @Enumerated(EnumType.ORDINAL)
    private Classe classe;

    
	@Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Carta{" +
            "nome='" + nome + '\'' +
            ", descricao='" + descricao + '\'' +
            ", defesa='" + defesa + '\'' +
            ", tipo='" + tipo + '\'' +
            ", classe='" + classe + '\'' +
            ", ataque='" + ataque + "'\'}";
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getAtaque() {
		return ataque;
	}

	public void setAtaque(Integer ataque) {
		this.ataque = ataque;
	}

	public Integer getDefesa() {
		return defesa;
	}

	public void setDefesa(Integer defesa) {
		this.defesa = defesa;
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}

	public Classe getClasse() {
		return classe;
	}

	public void setClasse(Classe classe) {
		this.classe = classe;
	}
    
    
}
