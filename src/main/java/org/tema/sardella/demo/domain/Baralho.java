package org.tema.sardella.demo.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.tema.sardella.demo.domain.enums.Classe;

import com.sun.istack.NotNull;

@Entity
@Table(name = "baralho")
public class Baralho  implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 1, max = 30)
    @Column(nullable = false)
    private String nome;

    @NotNull
    @Enumerated(EnumType.ORDINAL)
    private Classe classe;

    @OneToMany
    private List<Carta> cartas;
    
	@Override
    public int hashCode() {
        return 31;
    }

	@Override
	public String toString() {
		return "Baralho{" + "id='" + id + '\'' + "nome='" + nome + '\'' + "classe='" + classe + '\'' + ", cartas.size='"
				+ cartas.size() + "\'}";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Classe getClasse() {
		return classe;
	}

	public void setClasse(Classe classe) {
		this.classe = classe;
	}

	public List<Carta> getCartas() {
		return cartas;
	}

	public void setCartas(List<Carta> cartas) {
		this.cartas = cartas;
	}



}