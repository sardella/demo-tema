package org.tema.sardella.demo.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.tema.sardella.demo.domain.Carta;
import org.tema.sardella.demo.domain.enums.Classe;
import org.tema.sardella.demo.domain.enums.Tipo;
import org.tema.sardella.demo.repository.CartaRepository;
import org.tema.sardella.demo.rest.errors.CartaNaoEncontradaException;
import org.tema.sardella.demo.service.dto.CartaDTO;


@Service
@Transactional
public class CartaService {

    private final CartaRepository cartaRepository;

    public CartaService(CartaRepository cartaRepository) {
        this.cartaRepository = cartaRepository;
    }
    
    public Optional<CartaDTO> createCarta(CartaDTO cartaDTO) {

		Carta carta = new Carta();
		carta.setNome(cartaDTO.getNome());
		carta.setDescricao(cartaDTO.getDescricao());
		carta.setAtaque(cartaDTO.getAtaque());
		carta.setDefesa(cartaDTO.getDefesa());
		carta.setTipo(cartaDTO.getTipo());
		carta.setClasse(cartaDTO.getClasse());
		
		return Optional.of(cartaRepository.save(carta)).map(CartaDTO::new);
		
    }

    
    @Transactional(readOnly = true)
    public Page<CartaDTO> getCartas(Pageable pageable, String nome, Classe classe, Tipo tipo) {
    	
    	if(nome!=null)return cartaRepository.findByNomeContainingIgnoreCase(pageable, nome).map(CartaDTO::new);
    	
    	if(classe!=null)return cartaRepository.findByClasse(pageable, classe).map(CartaDTO::new);
    	
    	if(tipo!=null)return cartaRepository.findByTipo(pageable, tipo).map(CartaDTO::new);

    	return cartaRepository.findAll(pageable).map(CartaDTO::new);
    }
    
    @Transactional(readOnly = true)
    public Optional<CartaDTO> getOne(Long id) {
    	return Optional.of(cartaRepository.getOne(id)).map(CartaDTO::new);
    }
    
    public void deleteCarta(Long id) throws CartaNaoEncontradaException {
    	cartaRepository.findById(id).ifPresentOrElse(carta -> cartaRepository.delete(carta), 
    	        () -> {throw new CartaNaoEncontradaException();});
    }

}
