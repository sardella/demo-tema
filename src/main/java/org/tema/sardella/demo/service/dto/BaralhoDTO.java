package org.tema.sardella.demo.service.dto;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.tema.sardella.demo.domain.Baralho;
import org.tema.sardella.demo.domain.Carta;
import org.tema.sardella.demo.domain.enums.Classe;

public class BaralhoDTO {

	private Long id;

	@NotNull(message = "O Nome � obrigat�rio.")
	@Size(min = 1, max = 30, message= "O Nome deve ter no m�ximo 30 caracteres.")
	private String nome;
	
	@NotNull(message = "A Classe � obrigat�ria.")
	private Classe classe;
  
	@NotEmpty
    private List<Carta> cartas;

	public BaralhoDTO() {

	}

	public BaralhoDTO(Baralho baralho) {
		this.id = baralho.getId();
		this.nome = baralho.getNome();
		this.classe = baralho.getClasse();
		this.cartas = baralho.getCartas();
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Classe getClasse() {
		return classe;
	}

	public void setClasse(Classe classe) {
		this.classe = classe;
	}

	public List<Carta> getCartas() {
		return cartas;
	}

	public void setCartas(List<Carta> cartas) {
		this.cartas = cartas;
	}

	
	
}
