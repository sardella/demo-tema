package org.tema.sardella.demo.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.tema.sardella.demo.domain.Baralho;
import org.tema.sardella.demo.domain.enums.Classe;
import org.tema.sardella.demo.repository.BaralhoRepository;
import org.tema.sardella.demo.rest.errors.BaralhoNaoEncontradoException;
import org.tema.sardella.demo.rest.errors.LimiteBaralhoExcedidoException;
import org.tema.sardella.demo.service.dto.BaralhoDTO;


@Service
@Transactional
public class BaralhoService {

    private final BaralhoRepository baralhoRepository;

    public BaralhoService(BaralhoRepository baralhoRepository) {
        this.baralhoRepository = baralhoRepository;
    }
    
    public Optional<BaralhoDTO> createBaralho(BaralhoDTO baralhoDTO) throws LimiteBaralhoExcedidoException {

    	if(baralhoDTO.getCartas().size()>30) {
    		throw new LimiteBaralhoExcedidoException();
    	}
		Baralho baralho = new Baralho();
		baralho.setNome(baralhoDTO.getNome());
		baralho.setClasse(baralhoDTO.getClasse());
		baralho.setCartas(baralhoDTO.getCartas());
	
		return Optional.of(baralhoRepository.save(baralho)).map(BaralhoDTO::new);
		
    }

    
    @Transactional(readOnly = true)
    public Page<BaralhoDTO> getBaralhos(Pageable pageable, String nome, Classe classe) {
    	
    	if(nome!=null)return baralhoRepository.findByNomeContainingIgnoreCase(pageable, nome).map(BaralhoDTO::new);
    	
    	if(classe!=null)return baralhoRepository.findByClasse(pageable, classe).map(BaralhoDTO::new);
    	
    	return baralhoRepository.findAll(pageable).map(BaralhoDTO::new);
    }
    
    @Transactional(readOnly = true)
    public Optional<BaralhoDTO> getOne(Long id) {
    	return Optional.of(baralhoRepository.getOne(id)).map(BaralhoDTO::new);
    }
    
    public void deleteBaralhlo(Long id) throws BaralhoNaoEncontradoException {
    	baralhoRepository.findById(id).ifPresentOrElse(baralho -> baralhoRepository.delete(baralho), 
    	        () -> {throw new BaralhoNaoEncontradoException();});
    }


}
