package org.tema.sardella.demo.service.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.tema.sardella.demo.domain.Carta;
import org.tema.sardella.demo.domain.enums.Classe;
import org.tema.sardella.demo.domain.enums.Tipo;

public class CartaDTO {

	private Long id;

	@NotNull(message = "O Nome � obrigat�rio.")
	@Size(min = 1, max = 30, message= "O Nome deve ter no m�ximo 30 caracteres.")
	private String nome;

	@NotNull(message = "A Descri��o � obrigat�ria.")
	@Size(min = 1, max = 100, message="A descri��o ter no m�ximo 100 caracteres.")
	private String descricao;
	
	@Min(value = 0, message = "O valor m�nimo para ataque � 0.")
	@Max(value = 10, message = "O valor m�ximo para ataque � 10.")
	private int ataque;

	@Min(value = 0, message = "O valor m�nimo para defesa � 0.")
	@Max(value = 10, message = "O valor m�ximo para defesa � 10.")
	private int defesa;

	@NotNull(message = "O Tipo � obrigat�rio.")
	private Tipo tipo;
	
	@NotNull(message = "A Classe � obrigat�ria.")
	private Classe classe;

	public CartaDTO() {

	}

	public CartaDTO(Carta carta) {
		this.id = carta.getId();
		this.nome = carta.getNome();
		this.descricao = carta.getDescricao();
		this.ataque = carta.getAtaque();
		this.defesa = carta.getDefesa();
		this.tipo = carta.getTipo();
		this.classe = carta.getClasse();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public int getDefesa() {
		return defesa;
	}

	public void setDefesa(int defesa) {
		this.defesa = defesa;
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}

	public int getAtaque() {
		return ataque;
	}

	public void setAtaque(int ataque) {
		this.ataque = ataque;
	}

	public Classe getClasse() {
		return classe;
	}

	public void setClasse(Classe classe) {
		this.classe = classe;
	}

	

}
